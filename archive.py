from struct import pack
import os
import sys

def lowByte(m):
    return m & 0xff

def files(path):
    for file in os.listdir(path):
        if os.path.isfile(os.path.join(path, file)):
            yield file
            
directory = sys.argv[1]
fileMap = []
offset = 0
for fileName in files(directory):
    with open(os.path.join(directory, fileName), 'rb') as f:
        data = f.read()
        fileMap.append((fileName, offset, len(data), data))
        offset += len(data)
dataSize = offset

# gen header
HEADER_SIZE = 16
FS_ENTRY_SIZE = 32
numOfFiles = len(fileMap)
fsSize = FS_ENTRY_SIZE * (numOfFiles + 1)
header = pack('4sHHII', b'MRG\x00', 0x0, 0x0, fsSize + HEADER_SIZE, numOfFiles)

# gen file system
fileSystem = bytearray()
for fileName, offset, size, data in fileMap:
    entry = bytearray(FS_ENTRY_SIZE)
    entry[:len(fileName)] = fileName.encode('ascii')
    entry[14:18] = pack('I',size) 
    entry[18:20] = bytes(0) # no compression
    entry[28:32] = pack('I', offset + HEADER_SIZE + fsSize)
    fileSystem.extend(entry)

# add blank entry at the end of file system
blankEntry = bytearray(FS_ENTRY_SIZE)
blankEntry[28:32] = pack('I', dataSize + HEADER_SIZE + fsSize)
fileSystem.extend(blankEntry)

# encrypt file system
xorKey = 0
for i in range(fsSize):
    byte = fileSystem[i]
    byte = byte ^ xorKey
    byte = lowByte((byte >> 1) | (byte << 7)) # rotate right 1 bit
    fileSystem[i] = byte
    xorKey = xorKey + lowByte(fsSize-i)
    xorKey = lowByte(xorKey)

# write to archive
with open(f'_new_{directory}.MRG', 'wb') as f:
    f.write(header)
    f.write(fileSystem)
    for fileName, _, _, data in fileMap:
        print(f'Archieving {fileName}...')
        f.write(data)
