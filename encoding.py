specialChars = 'ạảãàáâậầấẩẫăắằặẳẵóòọõỏôộổỗồốơờớợởỡéèẻẹẽêếềệểễúùụủũưựữửừứíìịỉĩýỳỷỵỹđẠẢÃÀÁÂẬẦẤẨẪĂẮẰẶẲẴÓÒỌÕỎÔỘỔỖỒỐƠỜỚỢỞỠÉÈẺẸẼÊẾỀỆỂỄÚÙỤỦŨƯỰỮỬỪỨÍÌỊỈĨÝỲỶỴỸĐ'
replaceChars = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя≒≡∫√⊥∠∵∩∪∧∨⇒⇔∀┛┗┣┳┫┻╋┠┯┨┷┿┝┰┥┸αβγδεζηθικλμνξοπρστυφχψω┌┐┘└├┬┤┴┼━┃┏┓╂'

encodeTable = str.maketrans(specialChars, replaceChars)
decodeTable = str.maketrans(replaceChars, specialChars)

def encode(string):
    return string.translate(encodeTable).encode('shift-jis')

def decode(bytes):
    return bytes.decode('shift-jis').translate(decodeTable)
