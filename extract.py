from struct import unpack
import os
import sys

def lowByte(m):
    return m & 0xff

def getHighBitsOfWord(m, x):
    return m >> (16-x)

def getLowBitsOfWord(m, x):
    return m & ((1 << x) - 1)

def getByte(byteArr, offset):
    return byteArr[offset]

def getWord(byteArr, offset):
    return unpack('H', byteArr[offset:offset+2])[0]

def getDBWord(byteArr, offset):
    return unpack('I', byteArr[offset:offset+4])[0]

def getCString(byteArr):
     return byteArr.decode('ascii').replace('\x00', '')

def shl32(m, x):
    return (m << x) & 0xffffffff

def shr(m, x):
    return m >> x

fileName = sys.argv[1]
data = []
with open(fileName, 'rb') as f:
    data = bytearray(f.read())

# header
header = data[:16]
headerSize = len(header)

# check file type
fileType = header[:4]
if fileType != b'MRG\x00':
    raise Exception(f'Do not support this file type {fileType}')

# check num of files in archive
numOfFiles = getDBWord(header, 12)
if numOfFiles == 0:
    raise Exception('No files in archive')
print(f'Num of files in archive: {numOfFiles}')

# calculate XOR key
magicData1 = [ 0x00, 0x68, 0x5F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 ]
magicData2 = [ 0x00, 0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 ]
magicNum1 = getWord(header, 4)
magicNum2 = getWord(header, 6)
print(f'Magic Number 1 = {magicNum1:#0x}')
print(f'Magic Number 2 = {magicNum2:#0x}')
if magicNum1 >= len(magicData1) or magicNum2 >= len(magicData2):
    raise Exception(f'Magic numbers exceeds max supported value ({len(magicData1):#0x}, {len(magicData2):#0X})')
xorKey = lowByte(magicData1[magicNum1] + magicData2[magicNum2])
print(f'XOR key = {xorKey:#0x}')

# calculate size of map
sizeOfMap = getDBWord(header, 8) - headerSize
print(f'Size of Indexing: {sizeOfMap:#0x} bytes')

# decrypt indexing with XOR key
for i in range(sizeOfMap):
    byte = data[headerSize + i]
    byte = lowByte((byte << 1) | (byte >> 7)) # rotate left 1 bit
    byte = byte ^ xorKey
    data[headerSize + i] = byte
    xorKey = xorKey + lowByte(sizeOfMap-i)
    xorKey = lowByte(xorKey)

entrySize = 32
if sizeOfMap % entrySize != 0:
    raise Exception(f'Size of map not dividable by {entrySize}')

# parsing file mapping
fMap = {}
for i in range(0, sizeOfMap-entrySize, entrySize):
    entryOffset = headerSize + i;
    entry = data[entryOffset:entryOffset+entrySize]
    dataName = getCString(entry[:12])
    compressionType = getWord(entry, 18)
    dataOffset = getDBWord(entry, 28)
    if compressionType == 0:
        dataSize = getDBWord(entry, 14)
    else:
        nextEntryOffset = entryOffset + entrySize
        nextEntry = data[nextEntryOffset:nextEntryOffset+entrySize]
        nextDataOffset = getDBWord(nextEntry, 28)
        dataSize = nextDataOffset - dataOffset
    fMap[dataName] = (dataOffset, dataSize, compressionType)
if len(fMap) != numOfFiles:
    raise Exception(f'Num of files calculated from mapping ({len(fMap)}) differs from the one reported by archive ({numOfFiles})')

def decompress(dataName):
    dataOffset, dataSize, compressionType = fMap[dataName]
    rawData = data[dataOffset:dataOffset+dataSize]
    if compressionType == 0:
        return rawData
    elif compressionType == 1:
        return decompressLZSS(rawData)
    elif compressionType == 2:
        return decompressLZSS(decryptX(rawData))
    elif compressionType == 3:
        return decryptX(rawData)
    else:
        raise Exception(f'Unsupported compression type {compressionType}')
    
def decompressLZSS(compressedData):
    uncompressedData = bytearray()
    buf = [0] * 4096
    pos = 4078
    offset = 0
    while offset < len(compressedData):
        metaByte = compressedData[offset]
        offset += 1
        for i in range(8):
            if offset >= len(compressedData):
                break
            # check state bit
            isDataByte = metaByte & 1
            if isDataByte:
                byte = compressedData[offset]
                uncompressedData.append(byte)
                buf[pos] = byte
                pos = (pos + 1) % len(buf)
                offset += 1
            else:
                copyOperator = unpack('H', compressedData[offset:offset+2])[0]
                sizeToCopy = getHighBitsOfWord(copyOperator, 4) + 3
                fromPos = getLowBitsOfWord(copyOperator, 12)
                for j in range(sizeToCopy):
                    byte = buf[(fromPos + j) % len(buf)]
                    uncompressedData.append(byte)
                    buf[pos] = byte
                    pos = (pos + 1) % len(buf)
                offset += 2
            # shift to next state bit
            metaByte = metaByte >> 1
    return uncompressedData

def getMask(val):
    if val<=0x100:
        return 0xff
    elif val<=0x200:
        return 0x1ff
    elif val<=0x400:
        return 0x3ff
    elif val<=0x800:
        return 0x7ff
    elif val<=0x1000:
        return 0xfff
    elif val<=0x2000:
        return 0x1fff
    elif val<=0x4000:
        return 0x3fff
    elif val<=0x8000:
        return 0x7fff
    else:
        return 0xffff

def decryptX(bytearr):
    size = getDBWord(bytearr, 0) ^ getDBWord(bytearr, 260)
    orgVal = bytearr[4:260]
    aggSum = [ sum(orgVal[:i]) for i in range(len(orgVal)+1) ]
    table = []
    for i in range(0x100):
        table.extend([i]*orgVal[i])
    c = 0x10000 // aggSum[-1]
    mask = getMask(aggSum[-1])
    shift = unpack('>I', bytearr[260:264])[0]
    shiftPos = 264
    key1 = 0xffffffff
    key2 = 0

    res = []
    for i in range(size):
        key1 = shr(shr(key1, 8) * c, 8)
        val = table[(shift - key2) // key1]
        res.append(val)
        key2 += aggSum[val] * key1
        key1 *= orgVal[val]
        while not ((key1+key2) ^ key2) & 0xff000000:
            key1 = shl32(key1, 8) 
            key2 = shl32(key2, 8)
            shift = shl32(shift, 8) | bytearr[shiftPos]
            shiftPos += 1
        while not key1 & (~mask):
            key1 = shl32(~key2 & mask, 8)
            key2 = shl32(key2, 8) 
            shift = shl32(shift, 8) | bytearr[shiftPos]
            shiftPos += 1
    return bytearray(res)

nameWithoutExt = os.path.basename(fileName)
nameWithoutExt = os.path.splitext(nameWithoutExt)[0]
outDir = f'{nameWithoutExt}'
os.makedirs(outDir, exist_ok=True)
for dataName in fMap:
    outFile = os.path.join(outDir, dataName)
    with open(outFile, 'wb') as f:
        print(f'Extracting {dataName}...')
        f.write(decompress(dataName))
