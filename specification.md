## Header ##
Size: 16 bytes
4 first bytes = file type (const = 'MRG')
2 next bytes, 2 next bytes = pair of numbers to calculate xorKey
4 next bytes = size of file system
4 last bytes = num of files in archive

## File System ##
### Format ###
Start from the end of header and span across sizeOfFileSystem bytes
Each entry has size of 16 bytes and contains information about a file in the archive.
3 first dwords: name of file. 
dword[+?]: compression type (either 0, 1 or 2, > 2 is not confirmed?)
dword[1c]: offset of data in archive file

### Decryption ###
File system needs decrypting before reading. The algorithm for decryption is as follow: 
```
const1 = 0x1003BD58 (of system.unt)
const2 = 0x1003BD64 (of system.unt)
xorKey = byte[word[4]+const1] + byte[word[6]+const2] 
sizeOfFileSystem = byte(dword[8] - 16)
pos = 16

while sizeOfFileSystem > 0:
    data = byte[pos]
    data = rotateLeft(data, 1bit)
    data ^= xorKey
    result.append(data)
    xorKey += lowByte(sizeOfFileSystem)
    pos += 1
    sizeOfFileSystem -= 1
```
## File ##
Each file can be either not compressed at all, compressed using alogrithm type 1 or type 2.
### Alogrithm type 1 ###
#### Data Structure ####
[meta byte][byte][byte]...[byte]  [meta byte][byte][byte].....  [meta byte]......
[byte] can either be data or copy operator.
[meta byte] decides if which of subsequent bytes are data or copy operators
Copy operators are formed from two adjacent bytes: [byte][byte].

#### Metabyte ####
Example: 11110011
1st low bit = 1 -> the 1st byte after the meta byte is data byte
2nd low bit = 1 -> the 2nd byte after the meta byte is data byte
3rd low bit = 1 -> the 3rd byte after the meta byte is data byte
4th low bit = 1 -> the 4th byte after the meta byte is data byte
5th low bit = 0 -> the 5th & 6th bytes after the meta byte is a copy operator
6th low bit = 0 -> the 7th & 8th bytes after the meta byte is a copy operator
7nd low bit = 1 -> the 9th byte after the meta byte is data byte
8nd low bit = 1 -> the 10th byte after the meta byte is data byte
The 11th byte will be the next meta byte.

#### Copy operator ####
Using a cyclic buffer similar to LZ77 compression.
Format: [byte][byte]
Offset of data in buffer to copy: 12 low bit
How many bytes (size) to copy from offset position: [4 hight bit] + 3

#### Cyclic buffer ####
Spec: size = 4096, each element is a byte
Initialize the buffer with 0, start position = 0xfee
Each decompressed byte is put into the buffer

### Alogrithm type 2 ### 
- from +4 to (+4 + 0x100): byte arrays
- calculate array of aggressive sum from header, first element = 0, element size = dword
- init template 0...0{repeat = 0th byte of byte arrays) 1...1{repeat = 1stbyte of arrays) 2..2 3..3 ....

#### Transformation table for code pattern #####

- draw config:
  <block>
      ....
      ....
      ....
      <raw code=05/>
  </block

- draw instruction (inside draw config):
  <raw code="2fe2aa"/>
  <raw code="22e74c0403e40500000004e412000000" controlCode="04e412000000"/>
  <raw code="34e20be207e87a00e201"/>
  <raw code="2fe2ab"/>
  <raw code="05"/>

- excute drawing:
  <raw code="13e7e800b101"/>

- wait for input:
  <raw code="2fe203"/>


#### PARSING ARG & OPCODE #####

switch(opcode)
2: *
0..3: *
6..f: *
11:
13,14: same as argType = e1
12: *

A. check for gape8[8]:

B. op in 4..5
obj(pointer to the byte next to opCode, value=0,)
result=0
arg=?

C. op in15..7B:
struct = op : argType : arg : argType :argType : arg....
argType in (3,10) and (7b,) in order to be differentiable from next opcode

case argType:

- argType NOT in (3,10) and (7b,): end parsing

- e1: sub arguments
obj(?,?,)
subArgType in e2..e8
subArgTypePointer = argTypePointer + 1
while subArgType != 1:
    if not valid subArgType: break
    switch(subArgType):
        e2..e4: subArg = 2 or 3 or 4 byte, store at arr[numOfArgs] = (1, <1 or 2 or 4-byte value>)
        e5: subArg = null-terminated string, arr[numOfArgs] = (5, pointer to string arg)
        e6..e8: subArg = fixed 2 bytes, store at arr[numOfArgs] = (subArgType & 0f, <2-byte value>)
        otherwise: move to next byte 

- e2..e4: read 1, 2, 4 following bytes respectively as arg
arg = <1 or 2 or 4 next bytes>
obj(4, <1 or 2 or 4 next bytes>,)

- e5: 
arg = null-terminated string
obj(3, pointer to string arg)

- e6..e8:
arg = <2 next bytes>
obj(either {e6: 2, e7: 1, e8: 7}, pointer to probably 2 next bytes, )

- others: raise exception


#### PROCESS OPCODE #####
base address: 1003C590 for processing opcode
procedure for handling opcode = (base address + 4*opcode)

###### OPCODE = 22 ######
some checking...
03/04 E4 size ........... E4 size ......... E4 size ......... 05
      <--segment--------> <--segment------> <--segment------>

###### OPCODE = 23 ######
SEEMs to similar to op 22

###### OPCODE = 25 ######
25 03 E4 <size:dword> 14 ......... 04 E4 <size:dword> 14 ..........04 05
      <-------------segment---------> <-------------segment--------->
03 can be other byte in [0,3] and [10, 7b]
check if not 14:
    but 05: move to the byte after 05
    but !05: do some function?
check if not 04:
   end reading

###### OPCODE = 24 ######
SEEMs to similar to op 25
