import lxml.etree as xml
import sys
import os
import encoding
from struct import pack


def assemble(node):
    if node.tag == 'root':
        data = bytearray()
        for child in node:
            data += assemble(child)
        return data

    if node.tag == 'raw':
        return bytearray.fromhex(node.attrib['code'])

    if node.tag == 'text':
        return b'\x11' + encoding.encode(node.text) + b'\x00'

    if node.tag == 'block':
        data = bytearray()
        for child in node:
            data += assemble(child)
        data += b'\x05'
        size = len(data) + 4
        return bytearray.fromhex(node.attrib['code']) + pack('I', size) + data

    raise Exception(f'Unsupported tag {node.tag}')

def compile(node):
    reverseReduce(node)
    reverseTransform(node)
    return root

def replaceNode(node, xmlTemplate):
    newNodes = xml.fromstring('<root>' + xmlTemplate + '</root>').getchildren()
    parent = node.getparent()
    parent.replace(node, newNodes[0])
    for i in range(1, len(newNodes)):
        newNodes[i-1].addnext(newNodes[i])

def reverseReduce(root):
    for node in root:
        if node.tag == 'x-sentence':
            templ = f"""  
                <block code="2ee7e80003e4">
                    <color identifier="{node.attrib['colorId']}"/>
                    <raw code="13e60c00e200cb01"/>
                    <block code="2ee20103e4">
                      <text>{node.text}</text>
                    </block>
                    <raw code="2fe2aa"/>
                    <raw code="22e74c0403e40500000004e41200000034e20be207e87a00e2012fe2ab05"/>
                    <raw code="22e74c0403e40500000004e40f00000034e20be202e20fe7a00305"/>
                </block>
                <drawText/>
                <waitForInput/> 
            """
            replaceNode(node, templ)

        if node.tag == 'x-paragraph':
            templ = """
                <block code="2ee7e80003e4">
                <raw code="22e74c0403e40500000004e40d00000034e20be201e50a0005"/>
                <raw code="22e74c0403e41000000013e75004e212cb012ae20504e4080000002fe21205"/>
                </block>
                <drawText/>
            """
            replaceNode(node, templ)

        if node.tag == 'x-bgm':
            node.tag = node.tag.replace('x-', '')
            node.addnext(xml.Element('playBGM'))

        if node.tag == 'x-background':
            node.tag = node.tag.replace('x-', '')
            node.addnext(xml.Element('loadBackground'))

        if node.tag == 'x-sprite':
            node.tag = node.tag.replace('x-', '')
            node.addnext(xml.Element('loadSprite'))
            
        if node.tag == 'x-voice':
            node.tag = node.tag.replace('x-', '')
            node.addnext(xml.Element('playVoice'))

def reverseTransform(node):
    if node.tag in ['root', 'block']:
        for child in node:
            compile(child)

    if node.tag == 'bgm':
        node.tag = 'raw'
        node.attrib['code'] = '13e82100e5' + node.attrib['file'].encode('shift-jis').hex() + '00cb01'
    if node.tag == 'background':
        node.tag = 'raw'
        node.attrib['code'] = '13e81000e5' + node.attrib['file'].encode('shift-jis').hex() + '00cb01'
    if node.tag == 'voice':
        node.tag = 'raw'
        node.attrib['code'] = '13e81e00e5' + node.attrib['file'].encode('shift-jis').hex() + '00cb01'
    if node.tag == 'se':
        node.tag = 'raw'
        node.attrib['code'] = '13e81f00e5' + node.attrib['file'].encode('shift-jis').hex() + '00cb01'
    if node.tag == 'title':
        node.tag = 'raw'
        node.attrib['code'] = '13e85000e5' + node.attrib['text'].encode('shift-jis').hex() + '00cb01'
    if node.tag == 'jump':
        node.tag = 'raw'
        node.attrib['code'] = '13e81400e5' + node.attrib['script'].encode('shift-jis').hex() + '00e5' + node.attrib['ext'].encode('shift-jis').hex() + '00bbcb01' 
    if node.tag == 'color':
        node.tag = 'raw'
        node.attrib['code'] = '13e81d00e5' + node.attrib['identifier'].encode('shift-jis').hex() + '00cb01'
    if node.tag == 'waitForInput':
        node.tag = 'raw'
        node.attrib['code'] = '2fe203'
    if node.tag == 'drawText':
        node.tag = 'raw'
        node.attrib['code'] = '13e7e800b101'
    if node.tag == 'sprite':
        node.tag = 'raw'
        node.attrib['code'] = '13e81200e5' + node.attrib['file'].encode('shift-jis').hex() + '00cb01'
    if node.tag == 'playVoice':
        node.tag = 'raw'
        node.attrib['code'] = '22e74c0403e41000000013e75004e200cb012ae20504e4080000002fe20005'
    if node.tag == 'loadBackground':
        node.tag = 'raw'
        node.attrib['code'] = '22e74c0403e41000000013e75004e21ecb012ae20504e4080000002fe21e05'
    if node.tag == 'loadSprite':
        node.tag = 'raw'
        node.attrib['code'] = '22e74c0403e41000000013e75004e222cb012ae20504e4080000002fe22205'
    if node.tag == 'nextPage':
        node.tag = 'raw'
        node.attrib['code'] = '22e74c0403e41000000013e75004e216cb012ae20504e4080000002fe21605'
    if node.tag == 'playBGM':
        node.tag = 'raw'
        node.attrib['code'] = '22e74c0403e41000000013e75004e26ecb012ae20504e4080000002fe26e05'
    return node


filename = sys.argv[1]
nameWithoutExt = os.path.splitext(filename)[0]
with open(f'{nameWithoutExt}.MES', 'wb') as f:
    tree = xml.parse(filename)
    root = tree.getroot()
    root = compile(root)
    data = assemble(root)
    f.write(data)
