import lxml.etree as xml
import sys
import os
import glob
import re
import logging
from xml.dom import minidom 
from struct import unpack

def disassemble(data):
    """ 
    Applicable to any games using ADVWin32. 
    It disassemble a script into a mix of raw code and text strings in which text strings can be modified directly.
    """

    root = xml.Element('root')
    xml.SubElement(root, 'raw', name='header', code=data[:32].hex())
    disassembleSegment(root, data, 32, len(data))
    return root

def disassembleSegment(node, data, start, end):
    pos = start
    while pos < end:
        opcode = data[pos]
        if opcode == 2:
            xml.SubElement(node, 'raw', code=f'{opcode:02x}')
            pos += 1
        elif opcode in [0x4, 0x5]:
            xml.SubElement(node, 'raw', code=f'{opcode:02x}')
            pos += 1
        elif opcode == 0x11:
            initialPos = pos
            while pos < end and data[pos]:
                pos += 1
            if data[pos] and pos + 1 < end:
                raise Exception(f'Failed to determine end of string at position {initialPos}')
            pos += 1
            xml.SubElement(node, 'text').text = data[initialPos+1:pos-1].decode('shift-jis', errors='replace')
        elif opcode in [0x13, 0x14]:
            initialPos = pos
            pos += 1
            allTexts = []
            while data[pos] != 1:
                byte = data[pos]
                while byte >= 0xe2 and byte <= 0xe8:
                    if byte in [0xe2, 0xe3, 0xe4]:
                        pos += (byte & 0xf) + (byte == 0xe4)
                    elif byte == 0xe5:
                        text = bytearray()
                        allTexts.append(text)
                        if text:
                            text = bytearray()
                        while pos < end and data[pos]:
                            text.append(data[pos])
                            pos += 1
                        if data[pos]:
                            raise Exception(f'Failed to seek to zero byte')
                    elif byte in [0xe6, 0xe7, 0xe8]:
                        pos += 2
                    byte = data[pos]
                pos += 1
            pos += 1
            snode = xml.SubElement(node, 'raw', code=data[initialPos:pos].hex())
            for i, text in enumerate(allTexts):
                attrName = 'text' if i == 0 else 'text' + str(i)
                snode.attrib[attrName] = text[1:].decode('shift-jis')
        elif opcode in [0x22, 0x23, 0x24, 0x25, 0x2e, 0x34, 0x5a, 0x2f]:
            # read arguments
            initialPos = pos
            pos += opcode > 0x5
            argType = data[pos]
            while argType > 0x3 and (argType < 0x10 or argType > 0x7b):
                if argType == 0xe5:
                    while pos < end and data[pos]:
                        pos += 1
                    if data[pos]:
                        raise Exception(f'Failed to seek to zero byte')
                    pos += 1
                elif argType in [0x4, 0x5]:
                    break
                elif argType in [0xe2, 0xe3, 0xe4]:
                    pos += (argType & 0xf) + (argType == 0xe4)
                elif argType in [0xe6, 0xe7, 0xe8]:
                    pos += 3
                else:
                    raise Exception(f'Unsupported argType {argType} at position {pos}')
                argType = data[pos]

            # read subprocess if has
            if opcode in [0x2e]:
                if data[pos] != 3:
                    raise Exception(f'Following opcode 0x{opcode:x} at position {pos} must be byte=0x3')
                size = unpack('I', data[pos+2:pos+6])[0] - 4
                block = xml.SubElement(node, 'block', code=data[initialPos:pos+2].hex())
                pos += 6
                endPos = pos + size - 1
                if data[endPos] != 0x5:
                    raise Exception(f'Expected byte at position {endPos} to be 0x05 but got: 0x{data[endPos]:x}')
                disassembleSegment(block, data, pos, endPos)
                pos = endPos + 1
            elif opcode in [0x22, 0x23]:
                if data[pos] != 0x3:
                    raise Exception(f'Expected byte at position {pos} to be 0x3 but got: 0x{data[pos]:x}')
                while True:
                    pos += 1
                    if data[pos] != 0xE4:
                        raise Exception(f'Expected byte at position {pos} to be 0xE4 but got: 0x{data[pos]:x}')
                    size = unpack('I', data[pos+1:pos+5])[0]
                    pos += size
                    if data[pos] == 0x5:
                        pos += 1
                        break
                xml.SubElement(node, 'raw', code=data[initialPos:pos].hex())
            elif opcode in [0x24, 0x25]:
                pos += 1
                if data[pos] != 0xE4:
                    raise Exception(f'Expected byte at position {pos} to be 0xE4 but got: 0x{data[pos]:x}')
                while True:
                    pos += 1
                    size = unpack('I', data[pos:pos+4])[0]
                    if data[pos+4] not in [0x13, 0x14]:
                        raise Exception(f'Expected byte at position {pos} to be in 0x13..0x14 but got 0x{data[pos+4]:x}')
                    pos += size
                    if data[pos-1] != 0x4:
                        break                
                xml.SubElement(node, 'raw', code=data[initialPos:pos].hex())
            else:
                xml.SubElement(node, 'raw', code=data[initialPos:pos].hex())
        else:
            raise Exception(f'Unhandled opcode 0x{opcode:x} at position {pos}')

def clean(root):
    """ Lossy process. Raw code that can be filtered out without affecting the script will be removed """

    disposableCode = ['^13e81600e5.+00cb01', '^13e60c00e200cb01']
    disposableCode += ['13e79903e200cb01', '13e60600e201cb01', '34e20ee200', '13e79903e201cb01', '13e60600e201cb01', '22e74c0403e41000000013e75004e21acb012ae20504e4080000002fe21a05']
    disposableCode += ['13e60600e200cb01', '13e79903e202cb01']

    for node in root:
        if node.tag == 'block' and node.find('block') is None and not (node[0].tag == 'raw' and node[0].attrib['code'] == '22e74c0403e40500000004e40d00000034e20be201e50a0005'):
            node.getparent().remove(node)
        if node.tag == 'raw':
            for code in disposableCode:
                if re.match(code, node.attrib['code']):
                    node.getparent().remove(node)
                    break

def reduce(root):
    """ Many-to-One mapping from raw code to function"""

    mergeableTags = ['playVoice', 'playBGM', 'loadBackground', 'loadSprite', 'drawText', 'waitForInput']

    for node in root:
        if node.tag == 'block':
            # merge: text
            if node.find('block') is not None:
                color = node.find('color')
                texts = node.find('block').findall('text')
                # narration
                if color is not None and len(texts) > 0:
                    node.clear()
                    node.tag = 'x-sentence'
                    node.text = ''.join(map(lambda e: e.text, texts))
                    node.attrib['colorId'] = color.attrib['identifier']
                # start-of-dialogue
                elif color is not None:
                    node.getparent().remove(node)
                    colorId = color.attrib['identifier']
                # end-of-dialogue
                elif len(texts) > 0: 
                    node.clear()
                    node.tag = 'x-sentence'
                    node.text = '「' + ''.join(map(lambda e: e.text, texts))
                    node.attrib['colorId'] = colorId
                else:
                    raise Exception('Either text or color must exist')
            # merge new line
            elif node[0].tag == 'raw' and node[0].attrib['code'] == '22e74c0403e40500000004e40d00000034e20be201e50a0005':
                node.clear()
                node.tag = 'x-paragraph'

        # pair-tags
        if node.tag == 'voice':
            node.tag = 'x-voice'

        if node.tag == 'bgm':
            node.tag = 'x-bgm'

        if node.tag == 'background':
            node.tag = 'x-background'

        if node.tag == 'sprite':
            node.tag = 'x-sprite'

        for tag in mergeableTags:
            if re.match(tag, node.tag):
                node.getparent().remove(node)
                break

        
def transform(node):
    """ One-to-One mapping from disassembly code to function """

    if node.tag in ['root', 'block']:
        for child in node:
            transform(child)

    if node.tag == 'raw':
        if re.match('^13e82100e5.+00cb01', node.attrib['code']):
            node.tag = 'bgm'
            node.attrib['file'] = node.attrib['text']
            node.attrib.pop('code')
            node.attrib.pop('text')
        elif re.match('^13e81000e5.+00cb01', node.attrib['code']):
            node.tag = 'background'
            node.attrib['file'] = node.attrib['text']
            node.attrib.pop('code')
            node.attrib.pop('text')
        elif re.match('^13e81e00e5.+00cb01', node.attrib['code']):
            node.tag = 'voice'
            node.attrib['file'] = node.attrib['text']
            node.attrib.pop('code')
            node.attrib.pop('text')
        elif re.match('^13e81f00e5.+00cb01', node.attrib['code']):
            node.tag = 'se'
            node.attrib['file'] = node.attrib['text']
            node.attrib.pop('code')
            node.attrib.pop('text')
        elif re.match('^13e85000e5.+00cb01', node.attrib['code']):
            node.tag = 'title'
            node.attrib.pop('code')
        elif re.match('^13e81400e5.+00e5.+00bbcb01', node.attrib['code']):
            node.tag = 'jump'
            node.attrib['script'] = node.attrib['text']
            node.attrib['ext'] = node.attrib['text1']
            node.attrib.pop('code')
            node.attrib.pop('text')
            node.attrib.pop('text1')
        elif re.match('^13e81d00e5.+00cb01', node.attrib['code']):
            node.tag = 'color'
            node.attrib['identifier'] = node.attrib['text']
            node.attrib.pop('code')
            node.attrib.pop('text')
        elif re.match('^13e81200e5.+00cb01', node.attrib['code']):
            node.tag = 'sprite'
            node.attrib['file'] = node.attrib['text']
            node.attrib.pop('code')
            node.attrib.pop('text')
        elif re.match('22e74c0403e41000000013e75004e200cb012ae20504e4080000002fe20005', node.attrib['code']):
            node.tag = 'playVoice'
            node.attrib.pop('code')
        elif re.match('22e74c0403e41000000013e75004e21ecb012ae20504e4080000002fe21e05', node.attrib['code']):
            node.tag = 'loadBackground'
            node.attrib.pop('code')
        elif re.match('22e74c0403e41000000013e75004e222cb012ae20504e4080000002fe22205', node.attrib['code']):
            node.tag = 'loadSprite'
            node.attrib.pop('code')
        elif re.match('22e74c0403e41000000013e75004e26ecb012ae20504e4080000002fe26e05', node.attrib['code']):
            node.tag = 'playBGM'
            node.attrib.pop('code')
        elif re.match('22e74c0403e41000000013e75004e216cb012ae20504e4080000002fe21605', node.attrib['code']):
            node.tag = "nextPage"
            node.clear()    
        elif re.match('13e7e800b101', node.attrib['code']):
            node.tag = 'drawText'
            node.clear()
        elif re.match('2fe203', node.attrib['code']):
            node.tag = 'waitForInput'
            node.clear()
    
def decompile(node):
    """ Specific to Konakana, not applicable to other games """

    transform(node)
    reduce(node)
    clean(node)
    return node


path = sys.argv[1]
for filename in glob.glob(path):
    with open(filename, 'rb') as f:
        data = f.read()
    try:
        print(f'Decompiling {filename}...')
        root = disassemble(data)
        root = decompile(root)

        # write to file
        nameOnly = os.path.basename(filename)
        nameOnly = os.path.splitext(nameOnly)[0]
        with open(f'scripts/{nameOnly}.xml', 'wb') as f:
            xmlstr = xml.tostring(root, pretty_print=True, encoding='utf-8')
            f.write(xmlstr)
    except:
        logging.exception(f'Failed to decompile {filename}')
